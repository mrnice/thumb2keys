Thumb2keys
==========
About
-----
This is a copy of a very old project of me you can find [here](https://sourceforge.net/projects/slackpad/files/thumb2keys/)

Description
-----------

"Thumb to keys" lets you use the buttons of your PDA for fast writing.
First of all it translates the keypad direction into numbers
"up + left" is one, "up" is two, "up + right" is tree and so on. After that
it will allways look after combinations of two numbers to generate a key.
the definition of the keys is on thumbscript.com or you can read the keytable
struct.

It is able to switch the behaviour of the buttons. If you are pressing the
combination "9 4" in thumscript mode (the default mode on startup) you will
get into the normal mode. You than can use your buttons as usual. If you
want to get into the thumbscript mode you need to press enter 5 times.
To define another keycode to toggle use the -k option

As default thumb2keys uses the evdev file /dev/input/event0. If you
want to change this use the -e option.

Later there will be more possible keytables for your convenience.

You need to have uinput and evdev loaded or compiled in your kernel!

Have fun!

Some parts of this program are based on esekeyd, uinput_mouse.c and kbdd 
which are all licenced under GPL.

This Code is licenced under GPL v2.

Written by Bernhard Guillon <Bernhard.Guillon@opensimpad.org>

Install
-------
    meson build
    cd meson
    ninja