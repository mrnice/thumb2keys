/*
 * Header file
 *
 * This Code is licenced under GPL v2.
 *
 * Written by Bernhard Guillon <Bernhard.Guillon@opensimpad.org>
 *
*/
#include <linux/input.h>
#include <sys/types.h>

/*
 * Definitions for number_2_keyevents.c
*/
#define MAX_SPECIAL_KEYS 15
int pop(void);
void push(int i);
int switch_table(int a);
int numbers_to_keyevents(int a, int b);

/*
 * Definitions for keyevents_2_uinput.c
*/

#define KEY_PRESS   1
#define KEY_RELEASE 0
#define KEY_REPEAT  2

void key_press(int code);
void key_release(int code);
void key_autorepeat(int code);
int dev_uinput_init(void);

/*
 * Definitions for evdev_2_numbers
*/

void beginn_translation(void);
int dev_event_init(void);
