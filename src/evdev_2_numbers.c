/*
 * Evdev 2 numbers. This part of the programm will read out the evdev and
 * translate it to numbers (see main.c for explanation).
 *
 * This code is licenced under GPL v2.
 *
 * Written by Bernhard Guillon <Bernhard.Guillon@opensimpad.org>
 *
*/

#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>

#include "thumbkeys.h"

static void clear_kbuffer(void);
static void check_enter(struct input_event ev);
static void key_2_key(struct input_event ev);
static int key_to_number(struct input_event ev);

static struct input_event ev;
struct keybuffer {
	char up;
	char down;
	char left;
	char right;
	char enter;
};

static struct keybuffer kbuffer = { 0,0,0,0,0 };
static int numbers[2] = {0,0};
static int count_enter = 0;

int fp_event;
int used_keytable;
int table_switch;
char* eventfile;

char test_S = 55;

int dev_event_init(void)
{
	printf("EVENT %s",eventfile);
	if (!(fp_event = open(eventfile, O_RDONLY))) {
		perror("failed to open event device");
		return -1;
	}
	if ((ioctl(fp_event, EVIOCGRAB,1)) == -1) {
		perror("failed to grab the device");
		return -1;
	}
	return 0;
}

void clear_kbuffer(void)
{
	kbuffer.up = 0;
	kbuffer.down = 0;
	kbuffer.left = 0;
	kbuffer.right = 0;
	kbuffer.enter = 0;
}

int key_to_number(struct input_event ev)
{
	if (ev.value == 1) {
		if (ev.code == KEY_UP)
			kbuffer.up=1;
		else if (ev.code == KEY_DOWN)
			kbuffer.down=1;
		else if (ev.code == KEY_LEFT)
			kbuffer.left=1;
		else if (ev.code == KEY_RIGHT)
			kbuffer.right=1;
		else if (ev.code == table_switch)
			kbuffer.enter=1;
	}
	if (ev.value == 0) {
		if ( (kbuffer.up ==1) && (kbuffer.down ==0) && (kbuffer.left ==1) && (kbuffer.right ==0) && (kbuffer.enter ==0) ) {
			clear_kbuffer();
			return 1;
		}
		if ( (kbuffer.up ==1) && (kbuffer.down ==0) && (kbuffer.left ==0) && (kbuffer.right ==0) && (kbuffer.enter ==0) ) {
			clear_kbuffer();
			return 2;
		}
		if ( (kbuffer.up ==1) && (kbuffer.down ==0) && (kbuffer.left ==0) && (kbuffer.right ==1) && (kbuffer.enter ==0) ) {
			clear_kbuffer();
			return 3;
		}
		if ( (kbuffer.up ==0) && (kbuffer.down ==0) && (kbuffer.left ==1) && (kbuffer.right ==0) && (kbuffer.enter ==0) ) {
			clear_kbuffer();
			return 4;
		}
		if ( ( (kbuffer.up ==1) && (kbuffer.down ==1) && (kbuffer.left ==1) && (kbuffer.right ==1) ) || (kbuffer.enter ==1) ) {
			clear_kbuffer();
			return 5;
		}
		if ( (kbuffer.up ==0) && (kbuffer.down ==0) && (kbuffer.left ==0) && (kbuffer.right ==1) && (kbuffer.enter ==0) ) {
			clear_kbuffer();
			return 6;
		}
		if ( (kbuffer.up ==0) && (kbuffer.down ==1) && (kbuffer.left ==1) && (kbuffer.right ==0) && (kbuffer.enter ==0) ) {
			clear_kbuffer();
			return 7;
		}
		if ( (kbuffer.up ==0) && (kbuffer.down ==1) && (kbuffer.left ==0) && (kbuffer.right ==0) && (kbuffer.enter ==0) ) {
			clear_kbuffer();
			return 8;
		}
		if ( (kbuffer.up ==0) && (kbuffer.down ==1) && (kbuffer.left ==0) && (kbuffer.right ==1) && (kbuffer.enter ==0) ) {
			clear_kbuffer();
			return 9;
		}
	}

	return 0;
}

void check_enter(struct input_event ev)
{
	if ((ev.value == KEY_RELEASE) && (ev.code == table_switch)) {
		if (count_enter++ >=5) {
			used_keytable = 1;
			count_enter = 0;
		}
	}
	if ((ev.value == KEY_RELEASE) && (ev.code != table_switch))
		count_enter = 0;
}

void key_2_key(struct input_event ev)
{
	if (ev.value == KEY_PRESS)
		key_press(ev.code);
	if (ev.value == KEY_RELEASE)
		key_release(ev.code);
	if (ev.value == KEY_REPEAT)
		key_autorepeat(ev.code);
}

void beginn_translation(void)
{
	int number = 0;
	int i = 0;
	while (read (fp_event,&ev, sizeof (struct input_event))) {
		if (ev.type == EV_KEY) {
			check_enter(ev);
			if (used_keytable == 0)
				key_2_key(ev);
			else {
				number = key_to_number(ev);
				if (number != 0) {
					switch (i) {
					case 0:
						numbers[i] = number;
						i++;
						break;
					case 1:
						numbers[i] = number;
						i=0;
						numbers_to_keyevents(numbers[0],numbers[1]);
						break;
					}
				}
			}
		}
	}
}
