/*
 * "Thumb to keys" lets you use the buttons of your PDA for fast writing.
 * First of all it translates the keypad direction into numbers
 * "up + left" is one, "up" is two, "up + right" is tree and so on. After that
 * it will allways look after combinations of two numbers to generate a key.
 * the definition of the keys is on thumbscript.com or you can read the keytable
 * struct.
 *
 * It is able to switch the behaviour of the buttons. If you are pressing the
 * combination "9 4" in thumscript mode (the default mode on startup) you will
 * get into the normal mode. You than can use your buttons as usual. If you
 * want to get into the thumbscript mode you need to press enter 5 times.
 * To define another keycode to toggle use the -k option
 *
 * As default thumb2keys uses the evdev file /dev/input/event0. If you
 * want to change this use the -e option.
 *
 * Later there will be more possible keytables for your convenience.
 *
 * Use gcc -o thumb2keys keyevents_2_uinput.c \
 * number_2_keyevents.c evdev_2_nubers.c main.c
 * to compile it.
 *
 * You need to have uinput and evdev loaded or compiled in your kernel!
 *
 * Have fun!
 *
 * Some parts of this program are based on esekeyd, uinput_mouse.c and kbdd
 * which are all licenced under GPL.
 *
 * This Code is licenced under GPL v2.
 *
 * Written by Bernhard Guillon <Bernhard.Guillon@opensimpad.org>
 *
*/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "thumbkeys.h"

static void print_usage(void);

int used_keytable =1;
char* eventfile = "/dev/input/event0";
int table_switch = KEY_ENTER;
int fp_event=0;
int fp_uinput=0;

static void print_usage(void)
{
	printf("Usage: thumb2key [OPTION] [ARGUMENT]\n");
	printf("-e\t\t full path to the propper eventdev e.g. /dev/input/event0\n");
	printf("-k\t\t keycode, see input.h, used to switch to thumbscript \n\t\t behaviour. Default value is KEY_ENTER which is 28.\n");
	printf("-h\t\t this help\n");
}

int main(int argc, char** argv)
{
	int i;

	/*
	 * get the command line args
	 */

	for (i = 1; i < argc; i++) {
		if (argv[i][0] == '-') {
			switch (argv[i][1]) {
			case 'k':
				if (argv[++i] != NULL) table_switch = atoi(argv[i]);
				break;

			case 'e':
				if (argv[++i] != NULL) eventfile = argv[i];
				break;

			case 'h':
				print_usage();
				return 0;
				break;
			}
		}
	}

	/*
	 * set keytable to thumbscript table
	 */

	used_keytable=1;

	/*
	 * initialization of evdev and uinput and start of the translation
	 */

	if ( dev_event_init() || dev_uinput_init() )
		return -1;
	else
		beginn_translation();

	/*
	 * close uinput and evdev
	 */

	close(fp_uinput);
	close(fp_event);
	return 0;
}
