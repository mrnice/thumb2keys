/*
 * This is the number 2 keyevents part.
 * You can easily extend it or change the key-combinations.
 * Moreover it is planned to provide more keytables.
 * The key-combinations are similar to the ones of thumbscript.com with some
 * changes.
 *
 * The stack implementation is taken from programming C Kerninghan/Ritchie
 *
 * This Code is licenced under GPL v2.
 *
 * Written by Bernhard Guillon <Bernhard.Guillon@opensimpad.org>
 *
*/

#include <linux/input.h>
#include <stdio.h>

#include "thumbkeys.h"

//static int combo_key_counter=0;
//static int combo_key_code[10] = {0,0,0,0,0,0,0,0,0,0};
int used_keytable;

/* add a stack */
int sp = 0; /* set stack pointer to 0 */
int stack[MAX_SPECIAL_KEYS];

void push(int i)
{
	if(sp < MAX_SPECIAL_KEYS)
		stack[sp++] = i;
	else
		printf("ERROR: stack full");
}
int pop(void)
{
	if (sp > 0)
		return stack[--sp];
	else {
		printf("ERROR: stack empty");
		return 0;
	}
}
int switch_table(int a)
{

	/*
	* This part is expandable. It is planed to offer more than
	* one key-definition table. For that purpose we will save the table-number
	* in the value of key_shifter.
	*/

	switch (a) {
	case 0:
		used_keytable=0;
	}

	return 0;
}

int numbers_to_keyevents(int a, int b)
{
	/*static int combo_key_counter;
	static int combo_key_code[10];*/
	int item = 0;
	struct keycodes {
		int keycode;
		int key_shifter;
		int special;
	};
	struct keycodes keytable[9][9] = {
		{{KEY_LEFTCTRL,0,KEY_LEFTCTRL},{KEY_V,0,0},{KEY_W,0,0},{KEY_P,0,0},{KEY_1,0,0},{KEY_Q,0,0},{KEY_X,0,0},{KEY_Y,0,0},{KEY_U,0,0}},
		{{0,0,0},{KEY_LEFTSHIFT,0,KEY_LEFTSHIFT},{KEY_K,0,0},{KEY_J,0,0},{KEY_2,0,0},{KEY_L,0,0},{KEY_D,0,0},{KEY_I,0,0},{KEY_B,0,0}},
		{{KEY_APOSTROPHE,KEY_LEFTSHIFT,0},{KEY_APOSTROPHE,0,0},{KEY_BACKSPACE,0,0},{3,0,0},{KEY_3,0,0},{KEY_G,0,0},{KEY_E,0,0},{KEY_F,0,0},{KEY_C,0,0}},
		{{KEY_DOT,KEY_LEFTSHIFT,0},{KEY_EQUAL,0,0},{0,0,0},{KEY_TAB,0,0},{KEY_4,0,0},{KEY_O,0,0},{KEY_Z,0,0},{KEY_N,0,0},{KEY_H,0,0}},
		{{KEY_1,KEY_LEFTSHIFT,0},{KEY_2,KEY_LEFTSHIFT,0},{KEY_3,KEY_LEFTSHIFT,0},{KEY_4,KEY_LEFTSHIFT,0},{KEY_5,0,0},{KEY_6,KEY_LEFTSHIFT,0},{KEY_7,KEY_LEFTSHIFT,0},{KEY_8,KEY_LEFTSHIFT,0},{KEY_5,KEY_LEFTSHIFT,0}},
		{{KEY_ESC,0,0},{KEY_EQUAL,KEY_LEFTSHIFT,0},{KEY_COMMA,KEY_LEFTSHIFT,0},{KEY_0,0,0},{KEY_6,0,0},{KEY_ENTER,0,0},{0,0,0},{KEY_T,0,0},{KEY_S,0,0}},
		{{KEY_0,KEY_LEFTSHIFT,0},{KEY_SLASH,KEY_LEFTSHIFT,0},{KEY_SLASH,0,0},{KEY_SEMICOLON,0,0},{KEY_7,0,0},{0,0,0},{KEY_COMMA,0,0},{KEY_M,0,0},{KEY_A,0,0}},
		{{KEY_RIGHTBRACE,0,0},{KEY_BACKSLASH,KEY_LEFTSHIFT,0},{KEY_LEFTBRACE,0,0},{KEY_RIGHTBRACE,KEY_LEFTSHIFT,0},{KEY_8,0,0},{KEY_LEFTBRACE,KEY_LEFTSHIFT,0},{KEY_GRAVE,KEY_LEFTSHIFT,0},{KEY_SPACE,0,0},{KEY_R,0,0}},
		{{KEY_BACKSLASH,0,0},{0,0,0},{KEY_9,KEY_LEFTSHIFT,0},{99999,0,0},{KEY_9,0,0},{KEY_SEMICOLON,KEY_LEFTSHIFT,0},{KEY_MINUS,KEY_LEFTSHIFT,0},{KEY_MINUS,0,0},{KEY_DOT,0,0}}
	};

	/* We need to decrease the input values about one. This is because we get
	 * numbers from 1 to 9. We will also check that we get only those values
	*/

	if (((a >0) && (a<10)) && ((b >0) && (b<10))) {
		a--;
		b--;

		if (keytable[a][b].keycode !=0) {    /* Get rid of not defined combinations */

			/*
			 * If the keycode is 99999 we switch the keytable. I decided to use this
			 * because it is not defined. If I am wrong change it here and in the
			 * struct to some not defined value in input.h.
			*/
			//FIXME: take a deep look at the manual and rethink if using shift is needed
			if(keytable[a][b].keycode == 99999) {
				switch_table(keytable[a][b].key_shifter);
			} else  {
				if(keytable[a][b].key_shifter != 0)
					key_press(keytable[a][b].key_shifter);
				key_press(keytable[a][b].keycode);
				if(keytable[a][b].special==0) {
					key_release(keytable[a][b].keycode);
					if(keytable[a][b].key_shifter != 0)
						key_release(keytable[a][b].key_shifter);
					/* remove everything from stack because we had a normal key */
					item = pop();
					while (item !=0) {
						key_release(item);
						item = pop();
					}
				} else {
					/* push to stack */
					push(keytable[a][b].special);
				}
			}
		}
	}
	return 0;
}
