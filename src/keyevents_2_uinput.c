/*
 * Keyevents to uinput. This part creates a virtual keyboard for passing the
 * keycodes to the input device.
 *
 * This Code is licenced under GPL v2.
 *
 * Written by Bernhard Guillon <Bernhard.Guillon@opensimpad.org>
 *
*/

#include <linux/input.h>
#include <linux/uinput.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>

#include "thumbkeys.h"

int fp_uinput;

int dev_uinput_init(void)
{
	struct uinput_user_dev dev;
	int aux;

	fp_uinput = open("/dev/uinput", O_RDWR);
	if (fp_uinput <= 0)
		fp_uinput = open("/dev/misc/uinput", O_RDWR);
	if (fp_uinput <= 0)
		fp_uinput = open("/dev/devfs/misc/uinput", O_RDWR);
	if (fp_uinput <= 0)
		fp_uinput = open("/dev/input/uinput", O_RDWR);
	if (fp_uinput <= 0) {
		perror("failed to open the uinput device");
		return -1;
	}

	memset(&dev, 0, sizeof(dev));
	strncpy(dev.name, "Thumbscript", UINPUT_MAX_NAME_SIZE);


	if (write(fp_uinput, &dev, sizeof(dev)) < 0) {
		fprintf(stderr,"failed to write uinputdev");
		close(fp_uinput);
		return -1;
	}

	if (ioctl(fp_uinput, UI_SET_EVBIT, EV_KEY) != 0) {
		close(fp_uinput);
		return -1;
	}
	if (ioctl(fp_uinput, UI_SET_EVBIT, EV_REP) != 0) {
		close(fp_uinput);
		return -1;
	}
	for (aux = KEY_RESERVED; aux <= KEY_UNKNOWN; aux++)
		if (ioctl(fp_uinput, UI_SET_KEYBIT, aux) != 0) {
			close(fp_uinput);
			return -1;
		}
	if (ioctl(fp_uinput, UI_DEV_CREATE) != 0) {
		close(fp_uinput);
		return -1;
	}

	return 0;
}

void key_press(int code)
{
	struct input_event event;
	gettimeofday(&event.time, NULL);
	event.type = EV_KEY;
	event.code = code;
	event.value = KEY_PRESS;
	write(fp_uinput, &event, sizeof(event));
}

void key_release(int code)
{
	struct input_event event;
	gettimeofday(&event.time, NULL);
	event.type = EV_KEY;
	event.code = code;
	event.value = KEY_RELEASE;
	write(fp_uinput, &event, sizeof(event));
}

void key_autorepeat(int code)
{
	struct input_event event;
	gettimeofday(&event.time, NULL);
	event.type = EV_KEY;
	event.code = code;
	event.value = KEY_REPEAT;
	write(fp_uinput, &event, sizeof(event));
}
